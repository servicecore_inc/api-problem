<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * A 403 Forbidden problem response
 */
class Unauthorized extends Problem
{
    private const ERROR_CODE_KEY = 'errorCode';

    public function __construct(string $message = 'Not authorized', ?string $errorCode = null)
    {
        $additionalDetails = [self::ERROR_CODE_KEY => $errorCode];
        parent::__construct(
            new ApiProblem(403, $message, null, null, $additionalDetails)
        );
    }
}
