<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * A 401 "unauthenticated" problem response
 */
class Unauthenticated extends Problem
{
    public function __construct(string $message)
    {
        parent::__construct(
            new ApiProblem(401, $message)
        );
    }
}
