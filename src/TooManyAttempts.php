<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * A TooManyAttempts response should be returned when the user has sent too many
 * attempts to login in a given amount of time.
 */
class TooManyAttempts extends Problem
{
    /**
     * @param  int|null  $timeout  the user's timeout in seconds (optional)
     * @param  int|null  $attempt  the user's current attempt (optional)
     */
    public function __construct(int $timeout = null, int $attempt = null)
    {
        parent::__construct(
            new ApiProblem(
                429, 
                "Too many attempts",
                null, 
                null,
                ['timeout' => $timeout, 'attempt' => $attempt]
            )
        );

        if ($timeout !== null) {
            $this->getHeaders()->addHeaderLine('Retry-After', $timeout);
        }
    }
}
