<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * A 404 "not found" problem response
 */
class NotFound extends Problem
{
    public function __construct()
    {
        parent::__construct(
            new ApiProblem(404, 'The requested URL could not be matched by routing')
        );
    }
}
