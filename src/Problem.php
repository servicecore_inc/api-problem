<?php

namespace ServiceCore\ApiProblem;

use ZF\ApiProblem\ApiProblemResponse;

abstract class Problem extends ApiProblemResponse
{
    // nothing yet
}
