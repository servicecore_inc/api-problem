<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * A 423 locked response
 */
class Locked extends Problem
{
    public function __construct()
    {
        parent::__construct(
            new ApiProblem(423, 'The resource that is being accessed is locked')
        );
    }
}
