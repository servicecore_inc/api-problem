<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * A 422 "unprocessable entity" problem response
 */
class Unprocessable extends Problem
{
    /**
     * Called when the problem is constructed
     *
     * Keep in mind, $messages MUST adhere to the following format, where <field> is
     * the form field; <name> is the message template name (thanks Zend!); and, 
     * "message" is the validation message:
     *
     *     [
     *         "<field>": [
     *             "<name>": "<message>"
     *         ]
     *     ]
     *
     * @param  mixed[]  $messages  an array of validation messages
     */
    public function __construct(array $messages)
    {
        parent::__construct(
            new ApiProblem(
                422, 
                'Failed validation', 
                null, 
                null, 
                [
                    'validation_messages' => $messages
                ]
            )
        );
    }
}
