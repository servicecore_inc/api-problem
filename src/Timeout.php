<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * A timeout problem
 *
 * A timeout problem response should be returned when the user has sent too many
 * requests in a given amount of time. 
 */
class Timeout extends Problem
{
    /**
     * Called when the response is constructed
     *
     * @param  int|null  $timeout  the user's timeout in seconds (optional)
     */
    public function __construct(int $timeout = null)
    {
        parent::__construct(
            new ApiProblem(
                429, 
                "We're taking a breather", 
                null, 
                null,
                ['timeout' => $timeout]
            )
        );

        if ($timeout !== null) {
            $this->getHeaders()->addHeaderLine('Retry-After', $timeout);
        }
    }
}
