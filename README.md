# Problem

Generating custom API responses outside of ApiAgility can be a pain, especially `application/problem+json` responses with the correct HTTP status codes. 

You can use the following "templates" to quickly generate standard problem responses.

## 401 Not authorized

You can respond with a `401 Not Authorized` problem response (keep in mind, the HTTP `Not Authorized` status code is a mis-nomer, it should be called `Not Authenticated`):

```
// ... in a controller or resource action
return new Unauthenticated('foo');
```

The example above produces the following response:

```
{
    "title": "Unauthorized",
    "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
    "status": 401,
    "detail": "foo"
}
```

## 403 Fobidden

You can respond with a `403 Forbidden` problem response:

```
// ... in a controller or resource action
return new Unauthorized('foo');
```

The example above produces the following response:

```
{
    "title": "Forbidden",
    "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
    "status": 403,
    "detail": "foo"
}
```

## 404 Not Found

You can respond with a `404 Not Found` problem response:

```
// ... in a controller or resource action
return new NotFound();
```

The example above produces the following response:

```
{
    "title": "Not found",
    "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
    "status": 404,
    "detail": "The requested URL could not be matched by routing"
}
```

## 422 Unprocessable entity

You can respond with a `422 Unprocessable Entity` problem response:

```
// ... in a controller or resource action
return new Unprocessable(['foo' => ['bar' => 'baz']]);
```

The example above produces the following response:

```
{
    "title": "Unprocessable Entity",
    "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
    "status": 422,
    "detail": "Failed validation",
    "validation_messages": {
        "foo": {
            "bar": "baz"
        }
    }
}
```

## 423 Locked

You can respond with a `423 Locked` problem response:

```
// ... in a controller or resource action
return new Locked();
```

The example above produces the following response:

```
{
    "title": "Locked",
    "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
    "status": 423,
    "detail": "The resource that is being accessed is locked"
}
```

## 429 Too many requests

You can respond with a `429 Too Many Requests` problem response (with or without a timeout period in seconds):

```
//  ... in a controller or resource action
return new Timeout(10);
```

The example above produces the following response:

```
{
    "title": "Too Many Requests",
    "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
    "status": 429,
    "detail": "We're taking a breather",
    "timeout": 10
}
```

You can also respond with a `429 Too Many Requests` problem response and specify the attempt count:

```
//  ... in a controller or resource action
return new TooManyAttempts(10, 3);
```

The example above produces the following response:

```
{
    "title": "Too Many Requests",
    "type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
    "status": 429,
    "detail": "Too many attempts",
    "timeout": 10,
    "attempt": 3
}
```

In addition, if a timeout is given, the response will include the `Retry-After` header.

## Version

### 0.5.0, October 9, 2018

* Add _Too Many Attempts_ response

### 0.4.0, March 24, 2016

* Add _Too Many Requests_ response

### 0.3.0, March 22, 2016

* Add _Locked_ response

### 0.2.0, March 10, 2016

* Add _Unauthorized_ response
* Fix README's examples

### 0.1.0, March 7, 2016

* Initial release
