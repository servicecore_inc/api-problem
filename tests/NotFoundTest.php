<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * Tests for the unauthenticated response
 */
class NotFoundTest extends \PHPUnit_Framework_TestCase
{
    /* !__construct() */

    /**
     * __construct() should return response
     */
    public function test___construct_returnsResponse()
    {
        $response = new NotFound();

        $expected = new ApiProblem(
            404, 
            'The requested URL could not be matched by routing'
        );
        $actual = $response->getApiProblem();

        $this->assertEquals($expected, $actual);

        return;
    }
}
