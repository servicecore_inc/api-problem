<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;
use PHPUnit_Framework_TestCase as Test;

/**
 * Test for the Timeout problem response
 */
class TimeoutTest extends Test
{
    /* !__construct() */

    /**
     * __construct() should return an api problem response
     */
    public function test___construct_returnsResponse_ifTimeoutIsNull()
    {
        $expected = new ApiProblem(
            429,
            "We're taking a breather",
            null,
            null,
            ['timeout' => null]
        );

        $actual = (new Timeout())->getApiProblem();

        $this->assertEquals($expected, $actual);

        return;
    }

    /**
     * __construct() should return an api problem response
     */
    public function test___construct_returnsResponse_ifTimeoutIsNotNull()
    {
        $timeout = 999;

        // test to be sure the "Retry-After" header is set
        $response = new Timeout($timeout);
        $this->assertNotNull($response->getHeaders()->get('Retry-After'));

        // now, test the response's api problem
        $expected = new ApiProblem(
            429,
            "We're taking a breather",
            null,
            null,
            ['timeout' => $timeout]
        );
        $actual = $response->getApiProblem();
        $this->assertEquals($expected, $actual);

        return;
    }
}
