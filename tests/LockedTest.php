<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * Tests for the unauthenticated response
 */
class LockedTest extends \PHPUnit_Framework_TestCase
{
    /* !__construct() */

    /**
     * __construct() should return response
     */
    public function test___construct_returnsResponse()
    {
        $response = new Locked();

        $expected = new ApiProblem(
            423, 
            'The resource that is being accessed is locked'
        );
        $actual = $response->getApiProblem();

        $this->assertEquals($expected, $actual);

        return;
    }
}
