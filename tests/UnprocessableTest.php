<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * Tests for the unauthenticated problem response
 */
class UnprocessableTest extends \PHPUnit_Framework_TestCase
{
    /* !__construct() */

    /**
     * __construct() should return response
     */
    public function test___construct_returnsResponse()
    {
        $messages = ['foo' => ['bar' => 'baz']];

        $response = new Unprocessable($messages);

        $expected = new ApiProblem(
            422, 
            'Failed validation', 
            null, 
            null, 
            [
                'validation_messages' => $messages
            ]
        );
        $actual = $response->getApiProblem();

        $this->assertEquals($expected, $actual);

        return;
    }
}
