<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;
use PHPUnit_Framework_TestCase as Test;

class TooManyAttemptsTest extends Test
{
    public function test___construct_returnsResponse_ifTimeoutIsNull()
    {
        $expected = new ApiProblem(
            429,
            "Too many attempts",
            null,
            null,
            ['timeout' => null, 'attempt' => null]
        );

        $actual = (new TooManyAttempts())->getApiProblem();

        $this->assertEquals($expected, $actual);
    }

    public function test___construct_returnsResponse_ifTimeoutIsNotNull()
    {
        $timeout = 999;

        // test to be sure the "Retry-After" header is set
        $response = new TooManyAttempts($timeout);
        $this->assertNotNull($response->getHeaders()->get('Retry-After'));

        // now, test the response's api problem
        $expected = new ApiProblem(
            429,
            "Too many attempts",
            null,
            null,
            ['timeout' => $timeout, 'attempt' => null]
        );
        $actual = $response->getApiProblem();
        $this->assertEquals($expected, $actual);
    }

    public function test___construct_returnsResponse_ifAttemptIsNotNull()
    {
        $attempt = 3;

        // test to be sure the "Retry-After" header is set
        $response = new TooManyAttempts(null, $attempt);

        // now, test the response's api problem
        $expected = new ApiProblem(
            429,
            "Too many attempts",
            null,
            null,
            ['timeout' => null, 'attempt' => $attempt]
        );
        $actual = $response->getApiProblem();
        $this->assertEquals($expected, $actual);
    }
}
