<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * Tests for the unauthenticated response
 */
class UnauthenticatedTest extends \PHPUnit_Framework_TestCase
{
    /* !__construct() */

    /**
     * __construct() should return response
     */
    public function test___construct_returnsResponse()
    {
        $message = 'foo';

        $response = new Unauthenticated($message);

        $expected = new ApiProblem(401, $message);
        $actual   = $response->getApiProblem();

        $this->assertEquals($expected, $actual);

        return;
    }
}
