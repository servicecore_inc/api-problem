<?php

namespace ServiceCore\ApiProblem;

use Laminas\ApiTools\ApiProblem\ApiProblem;

/**
 * Tests for the un-authorized problem response
 */
class UnauthorizedTest extends \PHPUnit_Framework_TestCase
{
    /* !__construct() */

    /**
     * __construct() should set the response's message
     */
    public function test___construct_with_no_error_code_returnsResponse()
    {
        $message = 'foo';

        $response = new Unauthorized($message);
        $actual   = $response->getApiProblem();

        $expected = new ApiProblem(403, 'foo', null, null, ['errorCode' => null]);

        $this->assertEquals($expected, $actual);

        return;
    }

    public function test___construct_with_error_code_returnsResponse()
    {
        $message = 'foo';
        $errorCode = 'anErrorCode';

        $response = new Unauthorized($message, $errorCode);
        $actual   = $response->getApiProblem();

        $errorCode = ['errorCode' => $errorCode];
        $expected  = new ApiProblem(403, 'foo', null, null, $errorCode);

        $this->assertEquals($expected, $actual);

        return;
    }
}
